from math import floor, trunc
import numpy as np
import pandas as pd
from matplotlib import pyplot
from pandas import DataFrame

covid = DataFrame()
covid_biomarker = DataFrame()
covid_result = DataFrame()


def prepare_data(dataset, missing_values, padding_method):

    test_covid = pd.read_csv(dataset, sep=';', decimal=",")
    test_covid['RE_DATE'] = pd.to_datetime(test_covid['RE_DATE'])

    # drop patients without data
    test_covid['RE_DATE'].replace('', np.nan, inplace=True)
    test_covid.dropna(subset=['RE_DATE'], inplace=True)

    # Resample to get last test data
    covid = test_covid.groupby('PATIENT_ID').resample('Q', on='RE_DATE').last().reset_index(drop=True)
    covid = covid.drop(columns=['RE_DATE'])
    covid = covid.set_index('PATIENT_ID')

    patients = covid.shape[0]
    markers = covid.iloc[:, 5:].shape[1]

    # create empty lists for statistical info
    biomarker_means = []
    biomarker_nan = []
    biomarker_percent = []
    biomarker_names = []

    biomarkers = covid.iloc[:, 5:]

    # Fill statistical info
    for marker in biomarkers:
        biomarker_means.append(covid[marker].mean())
        biomarker_nan.append(covid[marker].isna().sum())
        biomarker_percent.append(round(covid[marker].isna().sum() / patients * 100, 1))
        biomarker_names.append(marker)

    df = DataFrame(np.array([biomarker_means, biomarker_nan, biomarker_percent]),
                   columns=biomarker_names)
    df = df.rename(index={0: 'mean', 1: 'NaN', 2: '% NaN'})

    # Merge the dataframes
    covid = covid.reset_index()
    result_nan = pd.concat([covid, df])

    # create deep copy to not affect original data
    result = covid.copy(deep=True)

    # Drop columns with more than missing_values % of NaN
    print('Drop columns with more than {}% of data missing ({} NaN values)'
          .format(missing_values, floor(patients * missing_values / 100)))
    print('Patients: {}, Missing data: {}, combined: {}'
          .format(patients, round(patients * missing_values / 100),
                  round(patients - patients * missing_values / 100)))

    result.dropna(thresh=round(patients - patients * missing_values / 100)+3, axis=1, inplace=True)

    # Drop rows with more than missing_values % of NaN
    print('Drop rows with more than {}% of data missing ({} NaN values)'.
          format(missing_values, round(markers * missing_values / 100)))
    result.dropna(thresh=floor(markers - markers * missing_values / 100), axis=0, inplace=True)

    print(result_nan)
    print(result)

    if padding_method == 1:
        # set remaining missing values to -1
        print('Set missing data to "-1"')
        result = result.fillna(-1)

    elif padding_method == 2:
        # set remaining missing values per column to column mean
        print('Set missing data to column mean')
        for marker in result.iloc[:, 5:]:
            result[marker].iloc[:, 5:].fillna(result[marker].loc['mean'], inplace=True)

    # Dump file to save data
    # result.to_csv('data/processed_data.csv',  decimal=",")
    # result.to_excel('data/processed_data_pruned_2.xlsx')

    global covid_result, covid_biomarker

    covid_result = result.drop(columns=['age', 'gender', 'Admission time', 'Discharge time'])
    covid_biomarker = covid_result.drop(columns=['outcome'])

    return covid_biomarker, covid_result


def print_data_info():

    print(covid_biomarker.head())
    print(covid_result.head())

    print(covid_result)
    print(covid_result.shape)
    print(list(covid_biomarker.columns))
    print(covid.describe())
    print(covid.groupby('outcome').size())


def data_plots():
    print('Making plots')

    for marker in covid_biomarker:
        fig, (ax1, ax2) = pyplot.subplots(ncols=2)
        ax1.set_xlabel('')
        ax1.set_ylabel('Measurement')
        ax2.set_xlabel('Measurement')
        ax2.set_ylabel('Frequency')
        fig.suptitle(marker)
        fig.tight_layout()
        covid_biomarker[marker].plot.box(ax=ax1)
        pyplot.title('')
        covid_biomarker[marker].hist(ax=ax2)  # wel handig voor het overzicht
        pyplot.savefig('data/images/%s.png' % str(marker))
        pyplot.show()

    pyplot.show()
    print('Done making plots')

