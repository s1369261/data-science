import pandas as pd
from matplotlib import pyplot
from pandas import DataFrame
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report, precision_score, recall_score, \
    f1_score
from sklearn.model_selection import train_test_split, StratifiedKFold, cross_val_score
from sklearn.linear_model import LogisticRegression
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier


def create_train_data(data):
    array = DataFrame(data)

    y = array.reset_index()
    y = y['outcome']
    X = array.drop(columns=['outcome', 'PATIENT_ID'])

    x_train, x_validation, y_train, y_validation = train_test_split(X, y, test_size=0.25, random_state=1)

    return [X, y, x_train, x_validation, y_train, y_validation]


def run_validation(x_train, x_validation, y_train, y_validation, model, method):

    # Spot Check Algorithms
    models = []
    cv_results_mean = []
    cv_results_std = []

    model_subset = ('LR', LogisticRegression(solver='liblinear', multi_class='ovr')), \
                   ('LDA', LinearDiscriminantAnalysis()), \
                   ('KNN', KNeighborsClassifier()), ('CART', DecisionTreeClassifier()), \
                   ('NB', GaussianNB())

    if model == 0:
        models.append(model_subset[0])
    elif model == 1:
        models.append(model_subset[1]) #even de vraag of dit bij binary classification past
    elif model == 2:
        models.append(model_subset[2])
    elif model == 3:
        models.append(model_subset[3])
    elif model == 4:
        models.append(model_subset[4])
    elif model == 5:
        models.append(model_subset[5])
    else:
        models = model_subset

    # evaluate each model in turn
    results = []
    names = []

    for name, model in models:
        kfold = StratifiedKFold(n_splits=10, random_state=1, shuffle=True)
        cv_results = cross_val_score(model, x_train, y_train, cv=kfold, scoring='accuracy')
        results.append(cv_results)
        cv_results_mean.append(cv_results.mean())
        cv_results_std.append((cv_results.std()))
        names.append(name)
        print('%s: %f (%f)' % (name, cv_results.mean(), cv_results.std()))

    # Compare Algorithms
    pyplot.boxplot(results, labels=names)
    pyplot.title('Algorithm Comparison')
    pyplot.show()

    if method == 'mean':
        best_model = model_subset[cv_results_mean.index(max(cv_results_mean))]
    elif method == 'std':
        best_model = model_subset[cv_results_std.index(min(cv_results_std))]

    predictions = pd.DataFrame(['accuracy', 'confusion matrix:', 'precision score:', 'recall:',
                                'f1-score:'])

    # Make predictions on validation dataset
    for model in models:
        print(model)
        test_model = model[1] #fill in the best model
        test_model.fit(x_train, y_train)
        prediction = test_model.predict(x_validation)

        acc = accuracy_score(y_validation, prediction)
        cm = confusion_matrix(y_validation, prediction)
        ps = precision_score(y_validation, prediction)
        re = recall_score(y_validation, prediction)
        f1 = f1_score(y_validation, prediction)

        predictions[model[0]] = [acc, cm, ps, re, f1]

    pd.set_option("display.max_rows", None, "display.max_columns", None)
    print(predictions.head())

        # Evaluate predictions
        # print('accuracy:', accuracy_score(y_validation, predictions))
        # print('confusion matrix:', confusion_matrix(y_validation, predictions))
        # print('classification report', classification_report(y_validation, predictions))
        # print('precision score:', precision_score(y_validation, predictions))
        # print('recall:', recall_score(y_validation, predictions))
        # print('f1-score:', f1_score(y_validation, predictions))
