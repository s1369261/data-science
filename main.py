import matplotlib.pyplot as plt

from models import create_train_data, run_validation
from process_data import prepare_data, data_plots
from feature_selection import rfe_ranking


decision_method = 'mean'  # 'mean' or 'std'
no_of_features = 7  # desired no of features for classifiers

# padding_method: 1 = -1; 2 = mean
covid_biomarker, covid_result = prepare_data(r'data/time_series_375_preprocess_en_csv.csv',
                                             missing_values=20, padding_method=1)

data_plots()

# train_data = create_train_data(covid_result)
# features = rfe_ranking(x=train_data[0], y=train_data[1], features=no_of_features,
#                        method=decision_method)

# # Copy the dataset and drop unnecessary biomarkers
# covid_selected = covid_result.copy(deep=True)
# drop_list = []
# for i in range(train_data[0].shape[1]):
#     if not features.support_[i]:
#         drop_list.append(covid_biomarker.iloc[:, i+1].name)
#
# covid_selected.drop(columns=drop_list, inplace=True)
# print('Dataset after removing unnecessary biomarkers:')
# print(covid_selected.head())
#
# train_data_selected = create_train_data(covid_selected)
#
# machine_learning = run_validation(x_train=train_data_selected[2],
#                                   x_validation=train_data_selected[3],
#                                   y_train=train_data_selected[4],
#                                   y_validation=train_data_selected[5], model=no_of_features,
#                                   method=decision_method)
