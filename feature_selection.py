import datetime

from numpy import mean
from matplotlib import pyplot
from numpy import std
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.feature_selection import RFE
from sklearn.linear_model import LogisticRegression, Perceptron
from sklearn.model_selection import RepeatedStratifiedKFold, cross_val_score
from sklearn.pipeline import Pipeline
from sklearn.tree import DecisionTreeClassifier


def rfe_ranking(x, y, features, method):
    # get a list of models to evaluate

    model_subset = [['per', Perceptron()], ['cart', DecisionTreeClassifier()],
                    ['rf', RandomForestClassifier()], ['gbm', GradientBoostingClassifier()]]

    score_means = []
    score_std = []
    best_classifier = []

    def get_models():
        models = dict()

        for classifier in model_subset:
            # print(classifier)
            rfe = RFE(estimator=classifier[1], n_features_to_select=5)
            model = classifier[1]
            models[classifier[0]] = Pipeline(steps=[('s', rfe), ('m', model)])
        return models

    # get a list of models to evaluate
    def get_best_model(classifier):
        models = dict()
        for i in range(6, 7):
            rfe = RFE(estimator=classifier, n_features_to_select=i)
            model = classifier
            models[str(i)] = Pipeline(steps=[('s', rfe), ('m', model)])
        return models

    # evaluate a give model using cross-validation
    def evaluate_model(model, x, y):
        cv = RepeatedStratifiedKFold(n_splits=10, n_repeats=3, random_state=1)
        scores = cross_val_score(model, x, y, scoring='accuracy', cv=cv, n_jobs=-1, error_score='raise')
        return scores

    def print_plots(model, outer_method):
        # get the models to evaluate
        if model == 0:
            models = get_models()
            pyplot.title('Best estimator for feature selection')
            pyplot.xlabel('Estimators')
            pyplot.ylabel('Accuracy')
        else:
            nonlocal best_classifier

            if outer_method == 'mean':
                best_classifier = model_subset[score_means.index(max(score_means))]
            elif outer_method == 'std':
                best_classifier = model_subset[score_std.index(min(score_std))]
            print(best_classifier)
            models = get_best_model(best_classifier[1])
            pyplot.title('Classification model: {}'.format(best_classifier[0]))
            pyplot.xlabel('Number of selected features')
            pyplot.ylabel('Accuracy')


        # evaluate the models and store results
        results, names = [], []
        for name, model in models.items():
            scores = evaluate_model(model, x, y)
            results.append(scores)
            names.append(name)
            score_means.append(mean(scores))
            score_std.append(std(scores))
            print('>%s %.3f (%.3f)' % (name, mean(scores), std(scores)))
        # plot model performance for comparison
        pyplot.boxplot(results, labels=names, showmeans=True)
        pyplot.show()
        pyplot.savefig('data/images/models/%s-%s.png' % (name, datetime.date.today()))

    print_plots(0, outer_method=method)
    print_plots(1, outer_method=method)

    # define RFE
    rfe = RFE(estimator=best_classifier[1], n_features_to_select=features)
    # fit RFE
    rfe.fit(x, y)
    # summarize all features
    for i in range(x.shape[1]):
        if rfe.support_[i]:
            print('Column: %s, Selected %s, Rank: %.3f' % (x.iloc[:, i].name, rfe.support_[i], rfe.ranking_[i]))

    return rfe